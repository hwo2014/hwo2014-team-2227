package noobbot;
class CarPositionMsgWrapper {
    public final String msgType;
    public final CarPositionData[] data;
    public final String gameId;
    public final int gameTick;

    CarPositionMsgWrapper(final String msgType, final CarPositionData[] data, 
    		final String gameId, final int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }
}

class CarPositionData {
	public final PlayerID id;
	public final double angle;
	public final CarPositionPiece piecePosition;
	
	public CarPositionData(final PlayerID id, final 
			double angle, final CarPositionPiece piecePosition) {
		this.id=id;
		this.angle=angle;
		this.piecePosition=piecePosition;
	}
}

class CarPositionPiece {
	public final double pieceIndex;
	public final double inPieceDistance;
	public final CarPositionLane lane;
	@Override
	public String toString() {
		return "[pieceIndex=" + pieceIndex
				+ ", inPieceDistance=" + inPieceDistance + ", lane=" + lane
				+ ", lap=" + lap + "]";
	}

	public final double lap;
	
	public CarPositionPiece(final double pieceIndex, final double 
			inPieceDistance, final CarPositionLane lane, final double lap) {
		this.pieceIndex=pieceIndex;
		this.inPieceDistance=inPieceDistance;
		this.lane=lane;
		this.lap=lap;
	}
}

class CarPositionLane {
	public final double startLaneIndex;
	public final double endLaneIndex;
	
	@Override
	public String toString() {
		return "[startLaneIndex=" + startLaneIndex
				+ ", endLaneIndex=" + endLaneIndex + "]";
	}

	public CarPositionLane(final double startLaneIndex, final double endLaneIndex) {
		this.startLaneIndex=startLaneIndex;
		this.endLaneIndex=endLaneIndex;
	}
}