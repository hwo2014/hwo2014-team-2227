package noobbot;
class InitMsgWrapper {
	public final String msgType;
	public final InitData data;

	InitMsgWrapper(final String msgType, final InitData data) {
		this.msgType = msgType;
		this.data = data;
	}
}

class InitData {
	public final InitRace race;

	InitData(final InitRace race) {
		this.race = race; 
	}
}

class InitRace {
	public final InitTrack track;
	public final InitCars[] cars;
	public final InitRaceSession raceSession;

	InitRace (final InitTrack track, final InitCars[] cars,
			final InitRaceSession raceSession) {
		this.track = track;
		this.cars = cars;
		this.raceSession = raceSession;
	}
}

class InitCars {
	public final PlayerID id;
	public final InitDimensions dimensions;

	InitCars (final PlayerID id, final InitDimensions dimensions) {
		this.id = id;
		this.dimensions = dimensions;
	}
}

class InitDimensions {
	public final double length;
	public final double width;
	public final double guideFlagPosition;

	InitDimensions (final double length, final double width, 
			final double guideFlagPosition) {
		this.length = length;
		this.width = width;
		this.guideFlagPosition = guideFlagPosition;
	}
}

class InitRaceSession {
	public final int laps;
	public final int maxLapTimeMs;
	public final boolean quickRace;

	InitRaceSession(final int laps, final int maxLapTimeMs,
			final boolean quickRace) {
		this.laps = laps;
		this.maxLapTimeMs = maxLapTimeMs;
		this.quickRace = quickRace;
	}
}