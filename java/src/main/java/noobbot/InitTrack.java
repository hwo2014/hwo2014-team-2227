package noobbot;
import java.util.Arrays;

class InitTrack {
	public final String id;
	public final String name;
	public final InitPiece[] pieces;
	public final InitLane[] lanes;
	public final InitStartingPoint startingPoint;
	private double[] baseSpeed;
	private double[] correctorSpeed;
	private boolean[] bestRight;
	private int[] plusCorrected;

	InitTrack (final String id, final String name, final InitPiece[] pieces, 
			final InitLane[] lanes, final InitStartingPoint startingPoint) {
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = lanes;
		this.startingPoint = startingPoint;
	}

	/**
	 * Doesn't consider radius on Bends 
	 */
	public void initSpeed1(){
		baseSpeed = new double[pieces.length];
		correctorSpeed = new double[pieces.length];		
		int a1, a2, a3, a4, b1, b2, b3;
		for (int i=0; i < pieces.length; i++){
			// pieces after 
			a1 = (i + 1) % pieces.length;
			a2 = (i + 2) % pieces.length;
			a3 = (i + 3) % pieces.length;
			a4 = (i + 4) % pieces.length;
			// pieces before
			b1 = (pieces.length + i - 1) % pieces.length;
			b2 = (pieces.length + i - 2) % pieces.length;
			b3 = (pieces.length + i - 3) % pieces.length;

			if (Math.abs(pieces[i].angle)>0) {
				baseSpeed[i] = 0.65; // on a turn
				if (Math.abs(pieces[a1].angle)==0)
					baseSpeed[i] += 0.1;
			} else if (Math.abs(pieces[a1].angle)>0)
				baseSpeed[i] = 1 - (Math.abs(pieces[a1].angle)/45)*
				(baseSpeed[b1]+baseSpeed[b2]+baseSpeed[b3]+0.6)/4; // before a turn
			else if (Math.abs(pieces[a2].angle)>0)
				baseSpeed[i] = 1 - (baseSpeed[b1]+baseSpeed[b2]+baseSpeed[b3]+0.7)/5; // 2 before a turn
			else if (Math.abs(pieces[a3].angle)>0)
				baseSpeed[i] = 1 - (baseSpeed[b1]+baseSpeed[b2]+baseSpeed[b3]+0.4)/6; // 3 before a turn
			else if (Math.abs(pieces[a4].angle)==0)
				baseSpeed[i] = 1;	// on a long straight line
			else 
				baseSpeed[i] = 0.853; // on a straight line

			//System.out.print(speed[i] + " - ");
		}
		System.out.println();
	}

	/**
	 * Considers Bends magnitude
	 */
	public void initSpeed2(){
		baseSpeed = new double[this.pieces.length];
		correctorSpeed = new double[this.pieces.length];		
		int k;
		InitPiece[] pieces = new InitPiece[7];

		for (int i=0; i < this.pieces.length; i++){
			for (int j=0; j<7; j++){
				k = (this.pieces.length + i + (j-3)) 
						% this.pieces.length;
				pieces[j] = this.pieces[k];
			} 
			k = 3; // pieces[3] is current 0..3..6

			if (pieces[k].getRadius()>0) {
				// in Bend --------------------------
				baseSpeed[i] = 0.35 + 0.35 *
						(pieces[k].getRadius() / 350);

				if (pieces[k+1].getRadius()==0)
					baseSpeed[i] += 0.12;
				else if (pieces[k].isRight()!=pieces[k+1].isRight())
					baseSpeed[i] += 0.05;
				else
					baseSpeed[i] -= 0.05 * 
					(200 - pieces[k+1].getRadius())/ 80;
				if (pieces[k-1].getRadius()>0)
					if (pieces[k].isRight()==pieces[k-1].isRight())
						baseSpeed[i] -= 0.07 * 
						(200 - pieces[k-1].getRadius())/ 100;
			} else 
				// in Straight ----------------------
				if (pieces[k+1].getRadius() + pieces[k+2].getRadius()+
						pieces[k+3].getRadius()==0)
					// long straight ahead
					baseSpeed[i] = 1;
				else if (pieces[k+1].getRadius()>0)
					// before a turn ----------------
					baseSpeed[i] = 0.55 + 0.25 *
					(pieces[k+1].getRadius() / 250);
				else if (pieces[k+2].getRadius()>0)
					// 2 before a turn --------------
					baseSpeed[i] = 0.7 + 0.25 *
					(pieces[k+1].getRadius() / 250);
				else if (pieces[k+3].getRadius()>0)
					// 3 before a turn --------------
					baseSpeed[i] = 0.8 + 0.15 *
					(pieces[k+1].getRadius() / 250);
				else
					baseSpeed[i] = 0.75;

			if (baseSpeed[i]>1) baseSpeed[i]=1;
			// System.out.print(baseSpeed[i] + " - ");
		}
		//System.out.println();
	}

	/**
	 * Init best lane to be in
	 */
	public void initLanes(){		
		bestRight = new boolean[pieces.length];
		plusCorrected = new int[this.pieces.length];
		// init primo pezzo
		for (int i=0; i<pieces.length; i++){
			if (pieces[i].angle>0){
				bestRight[0] = true;
				i=pieces.length;
			} else if (pieces[i].angle<0) {
				bestRight[0] = false;
				i=pieces.length;
			}
		}
		//System.out.print(bestRight[0] + " - ");

		// init backword
		for (int i=pieces.length-1; i>0; i--) {
			int k = (i+1)%pieces.length;
			int j = (i-1+pieces.length)%pieces.length;
			if (pieces[i].angle==0 || pieces[i].switc
					|| pieces[j].switc){
				bestRight[i] = bestRight[k];
			} else
				if (pieces[i].angle>0)
					bestRight[i] = true;
				else
					bestRight[i] = false;
			//System.out.print(bestRight[i] + " - ");
		}
	}

	public boolean goRight(double index){
		return bestRight[(int) index];
	}

	public double currentSpeed(double index){
		int i = (int) index;
		return baseSpeed[i] + correctorSpeed[i];
	}

	public void correctSpeed(double index, double angle, boolean crash){		
		int k = ((int) index - 1 + pieces.length) % pieces.length;
		
			if (crash || angle<25) {
				double p = Math.random()*0.35 + 0.25;
				if (!crash) {
					p *= (15-plusCorrected[k]*1.5)/20;
					if (angle==0) angle = 1;
					else angle = 40 / Math.abs(angle);
				} else {
					p *= -(18-plusCorrected[k]*1.5)/20;
					if (angle==0) angle = 1;
					else angle = 30 / Math.abs(angle);
				}
				int i = ((int) index - 1 + pieces.length) % pieces.length;
				int j = ((int) index - 2 + pieces.length) % pieces.length;

				correctorSpeed[i] += currentSpeed(i) * 0.15 * 
						(p * angle);
				if (currentSpeed(i) > 1)
					correctorSpeed[i] = 0.9 - baseSpeed[i];
				else if (currentSpeed(i) < 0)
					correctorSpeed[i] = 0.1 - baseSpeed[i];

				correctorSpeed[j] += currentSpeed(j) * 0.075 * 
						(p * angle);
				if (currentSpeed(j) > 1)
					correctorSpeed[j] = 0.9 - baseSpeed[j];
				else if (currentSpeed(j) < 0)
					correctorSpeed[j] = 0.1 - baseSpeed[j];

				if (!crash) 
					plusCorrected[k]++;
			}
	}

	@Override
	public String toString() {
		return "Track [id=" + id + ", name=" + name + ",\n pieces="
				+ Arrays.toString(pieces) + ",\n lanes=" + Arrays.toString(lanes)
				+ ",\n startingPoint=" + startingPoint + "]";
	}
}

class InitLane {
	public final double distanceFromCenter;
	public final double index;

	@Override
	public String toString() {
		return "distanceFromCenter=" + distanceFromCenter
				+ ", index=" + index;
	}

	InitLane (final double distanceFromCenter, final double index) {
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}
}

class InitStartingPoint {
	public final InitPosition position;
	public final double angle;

	InitStartingPoint (final InitPosition position, final double angle) {
		this.position = position;
		this.angle = angle;
	}

	@Override
	public String toString() {
		return "[position=" + position + ", angle=" + angle
				+ "]";
	}
}

class InitPosition {
	public final double x;
	public final double y;

	InitPosition (final double x, final double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "InitPosition [x=" + x + ", y=" + y + "]";
	}
}

class InitPiece {
	public final double length;
	public final boolean switc;
	public final double radius;
	public final double angle;

	@Override
	public String toString() {
		if (length!=0)
			return "[length=" + length + ", switch=" + switc + "]";
		else 
			return "[switch=" + switc + ", radius="
			+ radius + ", angle=" + angle + "]";
	}

	InitPiece (final double length, final boolean switc, 
			final double radius, final double angle) {
		this.length = length;
		this.switc = switc;
		this.radius = radius;
		this.angle = angle;
	}

	public double getLength() {
		return length;
	}

	public boolean isSwitch() {
		return switc;
	}

	/**
	 * @return Absolute value of radius
	 */
	public double getRadius() {
		return Math.abs(radius);
	}

	public boolean isRight() {
		return radius > 0;
	}

	/**
	 * @return Absolute value of angle
	 */
	public double getAngle() {
		return Math.abs(angle);
	}
}