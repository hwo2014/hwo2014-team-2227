package noobbot;

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class BotId{
	public final String name;
	public final String key;
	
	BotId(final String name, final String key) {
		this.name = name;
		this.key = key;
	}
}

class JoinData{
	public BotId botId;
	public String trackName;
	public int carCount;
}