package noobbot;

class JoinRace extends SendMsg {
	public final BotId botId;
	public final String trackName;
	public final int carCount;

	JoinRace(final BotId botId, final String trackName, 
			final int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.carCount = carCount;
	}

	@Override
	protected String msgType() {
		return "joinRace";
	}
}