package noobbot;

class LapFinished {
    public final String msgType;
	public final Lap data;
	public final String gameId;
	public final int gameTick;

	LapFinished(final String msgType, final Lap data,
			final String gameId, final int gameTick) {
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}

class Lap {
	public final PlayerID car;
	public final LapTime lapTime;
	public final Object raceTime;
	public final Object ranking;

	Lap(final PlayerID car, final LapTime lapTime, 
			final Object raceTime, final Object ranking) {
		this.car = car;
		this.lapTime = lapTime;
		this.raceTime = raceTime;
		this.ranking = ranking;
	}	
}

class LapTime {
	public final int lap;
	public final int ticks;
	public final int millis;

	LapTime(final int lap, final int ticks,
			final int millis) {
		this.lap = lap;
		this.ticks = ticks;
		this.millis = millis;
	}
}