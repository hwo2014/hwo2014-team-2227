package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import com.google.gson.Gson;

public class Main {
	final Gson gson = new Gson();
	private PrintWriter writer;
	private InitTrack track;
	private InitRaceSession raceSession;
	private PlayerID myCar;

	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new Main(reader, writer, new Join(botName, botKey));
		//new Main(reader, writer, new JoinRace(new BotId(botName, botKey), "keimola", 1));
		//new Main(reader, writer, new JoinRace(new BotId(botName, botKey), "keimola", 2));
		//new Main(reader, writer, new JoinRace(new BotId(botName, botKey), "keimola", 3));
		//new Main(reader, writer, new JoinRace(new BotId(botName, botKey), "germany", 1));
		//new Main(reader, writer, new JoinRace(new BotId(botName, botKey), "germany", 2));
		//new Main(reader, writer, new JoinRace(new BotId(botName, botKey), "usa", 1));
		//new Main(reader, writer, new JoinRace(new BotId(botName, botKey), "usa", 2));
		//new Main(reader, writer, new JoinRace(new BotId(botName, botKey), "france", 1));

	}

	public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
		this.writer = writer;
		String line = null;
		SwitchLane sLeft = new SwitchLane("Left");
		SwitchLane sRight = new SwitchLane("Right");
		double currentIndex = 0, angle = 0;

		send(join);

		while((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);


			if (msgFromServer.msgType.equals("carPositions")) {
				final CarPositionMsgWrapper pos = gson.fromJson(line, 
						CarPositionMsgWrapper.class);
				int k=0;
				for (int i=0; i < pos.data.length; i++)
					if (pos.data[i].id.color==myCar.color) {
						k = i;
						i = pos.data.length;
					}

				CarPositionPiece position = pos.data[k].piecePosition;
				angle = pos.data[k].angle;
				currentIndex = position.pieceIndex;

				double speed = track.currentSpeed(currentIndex);
				// position.inPieceDistance

				if (position.inPieceDistance<15 && position.inPieceDistance>10)
					if (track.goRight(currentIndex)) {
						if (track.lanes.length > position.lane.startLaneIndex) {
							send(sRight);
							//System.out.print("right ");
						}
					}else
						if (position.lane.startLaneIndex > 0) {
							send(sLeft);
							//System.out.print("left ");
						}

				if (position.inPieceDistance<11 && position.inPieceDistance>4)
					track.correctSpeed(currentIndex, angle, false);

				send(new Throttle(speed));
			} else {
				System.out.print(msgFromServer.msgType + " - ");
				if (msgFromServer.msgType.equals("join")) {
					System.out.print("Joined - ");
				} else if (msgFromServer.msgType.equals("gameInit")) {
					if (track == null) {
						line = line.replace("switch","switc");
						final InitMsgWrapper msgInit = gson.fromJson(line, 
								InitMsgWrapper.class);
						track = msgInit.data.race.track;            	
						raceSession = msgInit.data.race.raceSession;
						System.out.print("Race init - ");

						track.initSpeed2();
						track.initLanes();
						System.out.println("Track init"); 
					}
				} else if (msgFromServer.msgType.equals("gameEnd")) {
					System.out.println("Race end");
				} else if (msgFromServer.msgType.equals("gameStart")) {
					System.out.println("Race start");
				} else if (msgFromServer.msgType.equals("yourCar")) {
					YourCar msgCar = gson.fromJson(line, 
							YourCar.class);
					myCar = msgCar.data;
					System.out.print(myCar.color + " - ");
				} else if (msgFromServer.msgType.equals("turboAvailable")) {
					send(new Ping());
				} else if (msgFromServer.msgType.equals("lapFinished")) {
					LapFinished msgLap = gson.fromJson(line, 
							LapFinished.class);
					System.out.print("(" + 
							msgLap.data.lapTime.millis + "/" + 
							msgLap.data.lapTime.ticks + ") - ");
					send(new Ping());
				} else if (msgFromServer.msgType.equals("crash")) {
					track.correctSpeed(currentIndex, angle, true);
					System.out.print(" corrected speed - ");
				} else {
					send(new Ping());
				}
			}
		}
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
		//System.out.println(msg.toJson());
	}
}