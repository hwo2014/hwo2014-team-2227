package noobbot;

class PlayerID {
	public final String name;
	public final String color;
	
	public PlayerID (final String name, final String color) {
		this.name=name;
		this.color=color;
	}
}