package noobbot;

public class SwitchLane extends SendMsg {
	public final String direction;

	SwitchLane(final String direction) {
		this.direction = direction;
	}

	@Override
	protected Object msgData() {
        return direction;
    }
	
	@Override
	protected String msgType() {
		return "switchLane";
	}
}
