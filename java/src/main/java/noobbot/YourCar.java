package noobbot;

class YourCar {
    public final String msgType;
	public final PlayerID data;

	YourCar(final String msgType, final PlayerID data) {
		this.msgType = msgType;
		this.data = data;
	}
}